import * as cdk from '@aws-cdk/core';
import * as iam from '@aws-cdk/aws-iam'
import * as servicecatalog from '@aws-cdk/aws-servicecatalog';
import * as assets from '@aws-cdk/aws-s3-assets'
import * as path from 'path';

interface SCStackProps extends cdk.StackProps {
  sc_product_owner: string, 
  sc_product_distributor: string,
  sc_product_support_description: string,
  sc_product_support_email: string,
  sc_product_support_url: string,
  sc_portfolio_provider_name: string,
  sc_portfolio_description: string,
  sc_launch_role_arn: string,
  sc_baseline_template_filename: string,
}

export class ServiceCatalogCdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: SCStackProps) {
    super(scope, id, props);

    // Upload baseline and other CloudFormation templates as assets
    const baseline_asset = new assets.Asset(this, 'SampleAsset', {
      path: path.join(__dirname, props.sc_baseline_template_filename),
    });

    // Define execution role
    const clsc_launch_role = new iam.Role(this, 'CLSC_Launch_Role', {
      assumedBy: new iam.CompositePrincipal(
        new iam.ServicePrincipal('servicecatalog.amazonaws.com'),
        new iam.AccountPrincipal(cdk.Stack.of(this).account)
      ),
      description: 'Allows a Role to launch the service catalog product',
    });
    clsc_launch_role.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName('AdministratorAccess'))

    // Create baseline product
    const cfnBaselineProduct = new servicecatalog.CfnCloudFormationProduct(this, 'CLSC-Customer-Account-Baseline', {
      name: "CLSC-Customer-Account-Baseline",
      owner: props.sc_product_owner,
      distributor: props.sc_product_distributor,
      supportDescription: props.sc_product_support_description,
      supportEmail: props.sc_product_support_email,
      supportUrl: props.sc_product_support_url,
      provisioningArtifactParameters: [{
        info: { "LoadTemplateFromURL": baseline_asset.httpUrl },
        description: 'Deploy baseline in customer account',
        disableTemplateValidation: false,
        name: 'v1',
      }],
    });

    // Create Portfolio
    const cfnPortfolio = new servicecatalog.CfnPortfolio(this, 'CLSC-Customer-Account-Portfolio', {
      displayName: 'CLSC-Customer-Account-Portfolio',
      providerName: props.sc_portfolio_provider_name,
      description: props.sc_portfolio_description,
    });

    // Associate Portfolio and Produect(s)
    const cfnPortfolioProductAssociation = new servicecatalog.CfnPortfolioProductAssociation(this, 'CLSC-Customer-Account-Portfolio-Product-Association', {
      portfolioId: cfnPortfolio.ref,
      productId: cfnBaselineProduct.ref,
    });

    // Assign launch role
    const cfnPortfolioPrincipalAssociation = new servicecatalog.CfnPortfolioPrincipalAssociation(this, 'CLSC-Customer-Account-Principle-Association', {
      portfolioId: cfnPortfolio.ref,
      principalType: 'IAM',
      principalArn: clsc_launch_role.roleArn,
    });

  }
}
