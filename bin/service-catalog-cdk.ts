#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { ServiceCatalogCdkStack } from '../lib/service-catalog-cdk-stack';

const app = new cdk.App();
const sc_product_owner = app.node.tryGetContext('owner');
const sc_product_distributor = app.node.tryGetContext('distributor');
const sc_product_support_description = app.node.tryGetContext('support_description');
const sc_product_support_email = app.node.tryGetContext('support_email');
const sc_product_support_url = app.node.tryGetContext('support_url');
const sc_portfolio_description = app.node.tryGetContext('portfolio_description')
const sc_portfolio_provider_name = app.node.tryGetContext('portfolio_provider_name')
const sc_launch_role_arn = app.node.tryGetContext('launch_role_arn')
const sc_baseline_template_filename = app.node.tryGetContext('baseline_template_filename')

//Context parameters
const environment = app.node.tryGetContext('environment');

const scStack = new ServiceCatalogCdkStack(app, 'CLHS-ServiceCatalog-CustomerAccount-Stack', {
  env: environment,
  sc_product_owner: sc_product_owner,
  sc_product_distributor: sc_product_distributor,
  sc_product_support_description: sc_product_support_description,
  sc_product_support_email: sc_product_support_email,
  sc_product_support_url: sc_product_support_url,
  sc_portfolio_description: sc_portfolio_description,
  sc_portfolio_provider_name: sc_portfolio_provider_name,
  sc_launch_role_arn: sc_launch_role_arn,
  sc_baseline_template_filename: sc_baseline_template_filename,
});