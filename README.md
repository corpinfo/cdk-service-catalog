# Welcome to your CDK TypeScript project!

This project create the products and portfolio that are used for initialize the customer account.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

Edit the `cdk.context.json` file with environment specific values
* environment is the account and region to be initialized

## Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template

## Bootstrap example:

```export CDK_NEW_BOOTSTRAP=1
cdk bootstrap --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess \
aws://123456789012/us-east-1```

## Use Service Catalog

The service catalog portfolio is assigned to a launch role that is created by this CDK. The role is assumable to administrator user or role in the deployed account. Switch to the role before you launch product. 


## Reference Docs

 * [Simplify sharing your AWS Service Catalog portfolios in an AWS Organizations setup](https://aws.amazon.com/blogs/mt/simplify-sharing-your-aws-service-catalog-portfolios-in-an-aws-organizations-setup/)


